{
  description = "Cache of pre-built Genode binaries";

  edition = 201909;

  outputs = { self, nixpkgs }:
    let
      localSystem = "x86_64-linux";
      localPackages = nixpkgs.legacyPackages.${localSystem};

      unpack = baseUrl:
        { name, value }: {
          inherit name;
          value = localPackages.stdenvNoCC.mkDerivation {
            pname = name;
            inherit (value) version;
            preferLocalBuild = true;
            src = localPackages.fetchurl {
              url = "${baseUrl}${name}/${value.version}.tar.xz";
              inherit (value) sha256;
            };
            dontConfigure = true;
            dontBuild = true;
            installPhase = ''
              find . -type f | while read FILE; do
                install -Dm444 -t $out $FILE
              done
            '';
            meta = {
              license = [ "LicenseRef-Genode" ];
              downloadPage = "${baseUrl}${name}/";
            };
          };
        };

      expand = baseUrl: listing:
        builtins.listToAttrs (map (unpack baseUrl) listing);

      pkgs = expand "http://depot.genode.org/genodelabs/bin/x86_64/"
        (import ./genodelabs-list.nix);

    in {
      packages = {
        x86_64-linux-x86_64-genode = pkgs;
        x86_64-linux = pkgs;
      };
      checks = self.packages;
    };
}
